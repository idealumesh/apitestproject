package demo;
public enum apiEndPoints {

    //Host Detection
    Hosting_STATUS("https://www.who-hosts-this.com/API/Status?key="),
    Hosting_REQ("https://www.who-hosts-this.com/API/Host?key="),

    //Other APIs can be added here
    ReqRes_API("https://reqres.in/")
    ;


    private final String url;
    apiEndPoints(String url)
    {
        this.url = url;
    }
    public String getUrl()
    {
        return url;
    }
}
