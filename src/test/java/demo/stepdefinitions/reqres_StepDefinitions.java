package demo.stepdefinitions;

import demo.api_Controller.ReqRes.STATUS;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.hamcrest.core.Is;

import static demo.api_Controller.apiStates.RUNNING;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.assertj.core.api.Assertions.assertThat;

public class reqres_StepDefinitions {
    @Steps
    STATUS apiAppStatus;
    private String name;
    private String job;

    @Given("As a ReqRes tester I should able to access ResReq api")
    public void as_a_res_req_tester_i_should_able_to_access_the_res_req_api()
    {
        assertThat(apiAppStatus.currentStatus()).isEqualTo(RUNNING);
    }
    @When("I request for available users who are all registered")
    public void i_request_for_available_users_who_are_all_registered()
    {
        demo.api_Controller.ReqRes.GET.listuser(1);
    }
    @Then("I should be provided with first page of all users")
    public void i_should_be_provided_with_first_page_of_all_users()
    {
        restAssuredThat(lastResponse ->
        {
            lastResponse.body("page", Is.is(1)).log().all();
        });
    }
    @And("I request for page {int}")
    public void iRequestForPageNumber(int i)
    {
        demo.api_Controller.ReqRes.GET.listuser(i);
        restAssuredThat(lastResponse ->
        {
            lastResponse.body("page", Is.is(i)).log().all();
        });
    }

    @Then("I should be provided with page {int} of all users")
    public void i_should_be_provided_with_page_of_all_users(int i)
    {
        demo.api_Controller.ReqRes.GET.listuser(i);
        restAssuredThat(lastResponse ->
        {
            lastResponse.body("page", Is.is(i)).log().all();

        });
    }

    @When("I request for add a new user {string} with job {string}")
    public void i_request_for_add_a_new_user_with_job(String name, String job) {
        demo.api_Controller.ReqRes.POST.createUser(name,job);

    }

    @Then("I should see the user {string} gets created")
    public void i_should_see_the_user_is_get_created(String name) {
        restAssuredThat(lastResponse ->
        {
            lastResponse.body("name", Is.is(name)).log().all();

        });
    }
    @When("I request for update job title of an user {string} with job title {string}")
    public void i_request_for_update_job_title_of_an_user_with_job_title(String name, String job) {
        demo.api_Controller.ReqRes.PUT.updateUser(name,job);

    }
    @When("I request for patch job title of an user {string} with job title {string}")
    public void i_request_for_patch_job_title_of_an_user_with_job_title(String name, String job) {
        demo.api_Controller.ReqRes.PATCH.patchUser(name,job);

    }
    @Then("I should see the user {string} job title updated to {string}")
    public void i_should_see_the_user_job_title_updated_to(String name, String job) {
        restAssuredThat(lastResponse ->
        {
            lastResponse.body("name", Is.is(name)).log().all();

        });
        restAssuredThat(lastResponse ->
        {
            lastResponse.body("job", Is.is(job)).log().all();

        });

    }
    @When("I request for delete an user by ID {int}")
    public void i_request_for_delete_an_user_by_id(int id) {
        demo.api_Controller.ReqRes.DELETE.delID(id);
    }
    @Then("I should see the user get deleted")
    public void i_should_see_the_user_get_deleted() {
        restAssuredThat(response -> response.statusCode(204));
    }
    }
