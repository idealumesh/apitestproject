package demo.stepdefinitions;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import org.hamcrest.core.Is;
import demo.api_Controller.HostingPDR.STATUS;
import demo.api_Controller.HostingPDR.GET;
import static net.serenitybdd.rest.SerenityRest.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static demo.api_Controller.apiStates.RUNNING;

public class hosting_StepDefinitions {


    public String ky;

    @Steps
    STATUS apiApp;

    @Given("Host Detection api service")
    public void host_detection_api_service() {}

    @When("I check connecting to Host Detection api service")
    public void i_check_by_connecting_to_host_detection_api_service() {
        assertThat(apiApp.currentStatus(null)).isEqualTo(RUNNING);
    }

    @Given("As an enrolled user {string} of host detection service")
    public void as_an_enrolled_user_of_host_detection_service_with(String string) {
        // no Action required here
    }

    @When("I request status of service with {string}")
    public void api_status(String ky) {
        apiApp.readStatusMessage(ky);
    }

    @Then("The API service should respond to my request")
    public void api_response()
    {
        apiApp.readStatusMessage(ky);
        restAssuredThat(lastResponse -> lastResponse.statusCode(equalTo(200)));
    }

    @Then("The API service should return result with message as {string}")
    public void api_response_msg(String expectedMessage)
    {
        restAssuredThat(lastResponse -> {
            lastResponse.body("result.msg", Is.is(expectedMessage)).log().all();
        });
    }

    @Given("As an enrolled user <usr> of host detection service")
    public void asAnEnrolledUserUsrOfHostDetectionService() {
    }

    @When("I request hosting provider details of a website {string} with key {string}")
    public void i_request_hosting_provider_details_of_a_website(String webaddress, String k) {
        ky=k;
        GET.getHostingProvider(ky, webaddress);
        restAssuredThat(lastResponse -> {
            lastResponse.body("result.msg", Is.is("Success")).log().all();
        });

    }
    @Then("The API service should return details of hosting provider")
    public void the_api_service_should_return_details_of_hosting_provider() {
        restAssuredThat(lastResponse -> {
            lastResponse.body("result.msg", Is.is("Success")).log().all();
        });

    }
    @Then("provided details should match with test sample {string}")
    public void provided_details_should_match_with_test_sample(String string) {
        restAssuredThat(lastResponse -> {
            lastResponse.body("results.isp_name[0]", Is.is("Google Inc.")).log().all();
        });

    }
}
