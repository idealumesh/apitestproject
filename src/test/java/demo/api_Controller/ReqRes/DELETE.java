package demo.api_Controller.ReqRes;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import static demo.apiEndPoints.ReqRes_API;

public class DELETE {
    public DELETE(){
    }

    @Step("Delete a User using ID")
    public static void delID(int id)
    {
        SerenityRest.delete(ReqRes_API.getUrl()+ "api/users/" + id);
    }
}
