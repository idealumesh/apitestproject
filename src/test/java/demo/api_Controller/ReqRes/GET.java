package demo.api_Controller.ReqRes;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import static demo.apiEndPoints.ReqRes_API;

public class GET {


    public GET(){
    }

    @Step("Get Users list - pagewise")
    public static void listuser(int pagenumber)
    {
        SerenityRest.get(ReqRes_API.getUrl()+ "api/users/?page=" + pagenumber);
    }
}
