package demo.api_Controller.ReqRes;

import demo.api_Controller.apiStates;
import io.restassured.RestAssured;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import static demo.apiEndPoints.Hosting_STATUS;
import static demo.apiEndPoints.ReqRes_API;

public class STATUS {
    public STATUS()  {
    }
    public apiStates currentStatus() {
        int statusCode = RestAssured.get(ReqRes_API.getUrl() ).statusCode();
        return (statusCode == 200) ? apiStates.RUNNING : apiStates.DOWN;
    }
    @Step("Get current status message")
    public void readStatusMessage(String ky)
    {
        SerenityRest.get(ReqRes_API.getUrl());
    }
}
