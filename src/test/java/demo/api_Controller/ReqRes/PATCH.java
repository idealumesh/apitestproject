package demo.api_Controller.ReqRes;

import com.google.gson.Gson;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import static demo.apiEndPoints.ReqRes_API;

public class PATCH {
    @Step("Patch an Users(PATCH)")
    public static void patchUser(String usrname, String usrjob) {
        PATCH.User obj = new PATCH.User(usrname,usrjob);
        Gson gson = new Gson();
        String body = gson.toJson(obj);
        SerenityRest.given()
                .contentType("application/json")
                .header("Content-Type", "application/json")
                .body(body)
                .when()
                .patch(ReqRes_API.getUrl() + "api/users");

    }
    static class User {
        public String name;
        public String job;
        public User (String n,String j){
            this.name = n;
            this.job = j;
        }
    }
}
