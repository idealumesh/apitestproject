package demo.api_Controller.HostingPDR;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import static demo.apiEndPoints.Hosting_REQ;

public class GET {
    public GET(){
    }

    @Step("Get Hosting provider details")
    public static void getHostingProvider(String ky, String Url)
    {
        SerenityRest.get(Hosting_REQ.getUrl()+ ky + "&url=" + Url);
    }
}
