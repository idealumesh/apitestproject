package demo.api_Controller.HostingPDR;

import demo.api_Controller.apiStates;
import io.restassured.RestAssured;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import static demo.apiEndPoints.Hosting_STATUS;

public class STATUS {
    public STATUS()  {
    }
    public apiStates currentStatus(String ky) {
        int statusCode = RestAssured.get(Hosting_STATUS.getUrl()+ ky ).statusCode();
        return (statusCode == 200) ? apiStates.RUNNING : apiStates.DOWN;
    }
    @Step("Get current status message")
    public void readStatusMessage(String ky)
    {
        SerenityRest.get(Hosting_STATUS.getUrl()+ ky );
    }
}
