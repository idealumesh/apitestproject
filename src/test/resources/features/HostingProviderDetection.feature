@test
Feature: Host Provider Detection
  https://www.who-hosts-this.com api service

  @API_Status
  Scenario: Check if Host Detection api is online
    Given Host Detection api service
    When  I check connecting to Host Detection api service
    Then The API service should respond to my request

  @API_Authentication
  Scenario Outline: Check if Host Detection api can be Authenticated through valid API keys.
    Given As an enrolled user <usr> of host detection service
    When I request status of service with <key>
    Then The API service should return result with message as <response>
    Examples:
    |Key_Type|response|usr|key|
    |'Active API Key'|"Success"|"Umesh"|'7167d807a37521d2c0f459ee3a294e89dbbef6a095e9662d995e9da9883e021672f50e'|
    |'Inactive API Key'|"Invalid API Key"|"Kandhalu"|'716q7d807a37521d2c0f459ee3a294e89dbbef6a095e9662d995e9da9883e021672f50e'|

  @API_GetDetails
  Scenario Outline: Get the details of hosting provider of an website
    Given As an enrolled user <usr> of host detection service
    When I request hosting provider details of a website <website> with key <key>
    Then The API service should return details of hosting provider
    And  provided details should match with test sample <data_provider>
    Examples:
      |website|data_provider|key|
      |'www.google.com'|"Google Inc."|'7167d807a37521d2c0f459ee3a294e89dbbef6a095e9662d995e9da9883e021672f50e'|
#      |'www.yahoo.com'|"Yahoo"|'7167d807a37521d2c0f459ee3a294e89dbbef6a095e9662d995e9da9883e021672f50e'|