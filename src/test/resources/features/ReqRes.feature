
Feature: ReqRes User Management

  @GetUserList
  Scenario Outline: Check if registered users can be retrieved (GET Method)
    Given As a ReqRes tester I should able to access ResReq api
    When I request for available users who are all registered
    Then I should be provided with first page of all users
    And I request for page <number>
    Then I should be provided with page <number> of all users

    Examples:
    |number|
    |2     |
    |3     |

  @CreateUser
  Scenario: Create an User (POST Method)
    Given As a ReqRes tester I should able to access ResReq api
    When I request for add a new user "Umesh" with job "Tester"
    Then I should see the user "Umesh" gets created

  @UpdateUserDetails
  Scenario: Update an existing User details by replacing new user details (PUT method)
    Given As a ReqRes tester I should able to access ResReq api
    When I request for update job title of an user "Umesh" with job title "API Tester"
    Then I should see the user "Umesh" job title updated to "API Tester"

  @UpdateUserDetails
  Scenario: Update an existing User details by updating new user details (PATCH method)
    Given As a ReqRes tester I should able to access ResReq api
    When I request for patch job title of an user "Umesh" with job title "UI Tester"
    Then I should see the user "Umesh" job title updated to "UI Tester"

  @DeleteUser
  Scenario: Delete an existing User using id (DELETE method)
    Given As a ReqRes tester I should able to access ResReq api
    When I request for delete an user by ID 2
    Then I should see the user get deleted