# REST API testing with Serenity and Cucumber 6

This project will show how to Automate REST-APIs of https://www.who-hosts-this.com/ and https://reqres.in
Few sample test cases provided in BDD with Serenity using Cucumber 6. 

This project includes scripts for CI pipeline integration with GITLAB.

## How to get project and use
Clone the project 
```shell
git clone https://gitlab.com/idealumesh/apitestproject.git
cd apitestproject
```
To run the project
```shell
mvn clean verify
```
## A simple scenarios
The project comes with simple scenarios to test the https://www.who-hosts-this.com/ and https://reqres.in
API references can be obtained from 

1.https://www.who-hosts-this.com/Documentation
Note : You need to register at https://www.who-hosts-this.com and obtained API keys.

2.https://reqres.in


```Gherkin

API : Hosting Service Detector
There are two end points one for getting status of service and other one is to retrive the hosting provider data.
   
Scenario: Check if Host Detection api is online (https://www.who-hosts-this.com)
Given Host Detection api service
When  I check connecting to Host Detection api service
Then The API service should respond to my request

Scenario Outline: Get the details of hosting provider of an website
Given As an enrolled user <usr> of host detection service
When I request hosting provider details of a website <website> with key <key>
Then The API service should return details of hosting provider
And  provided details should match with test sample <data_provider>
Examples:
|website|data_provider|key|
|'www.google.com'|"Google Inc."|'<API Key>'|
|'www.yahoo.com'|"Yahoo"|'<API Key>'|

API : resreq fack API
@GetUserList
Scenario Outline: Check if registered users can be retrieved (GET Method)
Given As a ReqRes tester I should able to access ResReq api
When I request for available users who are all registered
Then I should be provided with first page of all users
And I request for page <number>
Then I should be provided with page <number> of all users

Examples:
|number|
|2     |
|3     |

@CreateUser
Scenario: Create an User (POST Method)
Given As a ReqRes tester I should able to access ResReq api
When I request for add a new user "Umesh" with job "Tester"
Then I should see the user "Umesh" gets created

@UpdateUserDetails
Scenario: Update an existing User details by replacing new user details (PUT method)
Given As a ReqRes tester I should able to access ResReq api
When I request for update job title of an user "Umesh" with job title "API Tester"
Then I should see the user "Umesh" job title updated to "API Tester"

@UpdateUserDetails
Scenario: Update an existing User details by updating new user details (PATCH method)
Given As a ReqRes tester I should able to access ResReq api
When I request for patch job title of an user "Umesh" with job title "UI Tester"
Then I should see the user "Umesh" job title updated to "UI Tester"

@DeleteUser
Scenario: Delete an existing User using id (DELETE method)
Given As a ReqRes tester I should able to access ResReq api
When I request for delete an user by ID 2
Then I should see the user get deleted

```

GitLab
This project is integrated with simple easy to understand CI/CD pipeline through the yml file .gitlab-ci.yml

.gitlab-ci.yml

```yaml

image: maven:3-jdk-8
Clean:
   stage: build
   script:
      - mvn clean
build _and_Test:
   stage: test
   script:
      - mvn verify
   artifacts:
      paths:
         - target/site/serenity/index.html
         when: always
```

To view the pipeline navigate to https://gitlab.com/idealumesh/apitestproject/-/pipelines.

Reports
1. Serenity reports can be viewed through artifacts
2. Navigate to below path once artifacts are loaded after pipeline run
   artifacts/target/site/serenity/index.html
   


